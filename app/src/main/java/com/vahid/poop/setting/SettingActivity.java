package com.vahid.poop.setting;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vahid.poop.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingActivity extends AppCompatActivity {

    @BindView( R.id.seekbar_setting_title  )
    SeekBar seekBar;

    @BindView( R.id.btn_save_setting )
    Button btn_save;

    @BindView( R.id.tv_sample )
    TextView tv_sample;

    @BindView( R.id.tv_sample_size )
    TextView tv_size;

    SharedPreferences sharedPreferences;
    int size=14;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_setting );
        ButterKnife.bind( this );

        sharedPreferences=getSharedPreferences( "setting",MODE_PRIVATE );
        size=sharedPreferences.getInt( "title_size",14 );
        seekBar.setProgress( size );
        tv_sample.setTextSize( size );
        tv_size.setText( size+" " );

        seekBar.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                tv_size.setText( i +" " );
                tv_sample.setTextSize( i );
                 size=i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        } );

        btn_save.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              sharedPreferences.edit().putInt( "title_size",size ).apply();
                Toast.makeText( getApplicationContext(), "تغییرات مورد نظر ذخیره شد.", Toast.LENGTH_SHORT ).show();

            }
        } );
    }
}
