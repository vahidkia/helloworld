package com.vahid.poop.main;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vahid.poop.R;
import com.vahid.poop.db.Product;

import java.util.ArrayList;
import java.util.List;

public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.Holder> {

    Context context;
    List<Product>products;
    OnclickListener onclickListener;
    SharedPreferences sharedPreferences;


    public AdapterProduct(Context context, List<Product>products,OnclickListener onclickListener){
        this.products=products;
        this.context=context;
        this.onclickListener=onclickListener;
        sharedPreferences=context.getSharedPreferences( "setting",Context.MODE_PRIVATE );
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from( context ).inflate( R.layout.custom_product,parent,false );
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        holder.tv_name.setText( products.get( position ).getName() );
        holder.tv_name.setTextSize( sharedPreferences.getInt( "title_size",14 ) );


        holder.tv_price.setText( products.get( position ).getPrice() + "تومان" );
        holder.tv_inventory.setText( products.get( position ).getInventory()+"عدد" );


        if(products.get( position ).getPic()!=null) {
            holder.iv_product.setImageURI( Uri.parse( products.get( position ).getPic() ) );
        }else {
            holder.iv_product.setImageResource( R.drawable.milk );
        }




    }

    @Override
    public int getItemCount() {
        return products.size();
    }


    public interface OnclickListener {       // vaghti ruye mahsul kilik kardim id anra begirim.

        public void itemClicked (int position);

    }



    class Holder extends RecyclerView.ViewHolder{

        ImageView iv_product;
        TextView tv_name,tv_inventory,tv_price;


       public Holder(@NonNull View itemView) {
           super( itemView );
           iv_product=itemView.findViewById( R.id.iv_product );
           tv_name=itemView.findViewById( R.id.tv_product_name );
           tv_inventory=itemView.findViewById( R.id.tv_inventory );
           tv_price=itemView.findViewById( R.id.tv_price_product);

           itemView.setOnClickListener( new View.OnClickListener() {
               @Override
               public void onClick(View view) {

                   onclickListener.itemClicked( getAdapterPosition() );  //tahvil dadane position be interface.

               }
           } );
       }
   }

}
