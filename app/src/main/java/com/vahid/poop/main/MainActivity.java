package com.vahid.poop.main;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Insert;
import androidx.room.Room;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vahid.poop.Product.NewProductActivity;
import com.vahid.poop.R;
import com.vahid.poop.category.CategoryActivity;
import com.vahid.poop.categoryfactor.CategoryFactorActivity;
import com.vahid.poop.db.Category;
import com.vahid.poop.db.Categoryfactor;
import com.vahid.poop.db.Dao;
import com.vahid.poop.db.MyDatabase;
import com.vahid.poop.db.NewFactor;
import com.vahid.poop.db.Product;
import com.vahid.poop.detail.DetailActivity;
import com.vahid.poop.mainfactor.MainFactorActivity;
import com.vahid.poop.profile.ProfileActivity;
import com.vahid.poop.setting.SettingActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements AdapterProduct.OnclickListener, MainCategoryAdapter.OnCategoryClick {

    DrawerLayout drawerLayout;
    ImageView iv_hum;
    ImageView iv_close_drawer;
    ImageView iv_profile;
    SharedPreferences sharedPreferences;

    TextView tv_category;

    ConstraintLayout cl_product;

    MyDatabase myDatabase;

    RecyclerView rv_categories;

    /*ImageView iv_close_profile;*/

    List<Category>categories=new ArrayList<>();

    MainCategoryAdapter mainCategoryAdapter;

    RecyclerView rv_products;
    AdapterProduct adapterProduct;
    List<Product>products=new ArrayList<>(  );



    Dao dao;

    ConstraintLayout cl_micr;

    EditText et_search;

    TextView tv_no_item;

    @BindView( R.id.tv_setting )
    TextView tv_setting;

    @BindView( R.id.tv_category_factor )
    TextView tv_facor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main);

        ButterKnife.bind(this);

        sharedPreferences=getSharedPreferences( "setting",MODE_PRIVATE );



       myDatabase= Room.databaseBuilder( MainActivity.this,
                MyDatabase.class,"anbar")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();

          dao=myDatabase.getDao();
          cl_micr=findViewById( R.id.cl_mic );
          tv_no_item=findViewById( R.id.tv_no_item );


          tv_facor.setOnClickListener( new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  startActivity( new Intent( MainActivity.this,MainFactorActivity.class ) );
              }
          } );

          cl_micr.setOnClickListener( new View.OnClickListener() {
              @Override
              public void onClick(View view) {

                  Intent intent = new Intent( RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                  intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                          RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                  intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                  intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                          getString(R.string.stt));
                  try {
                      startActivityForResult(intent, 100);
                  } catch (ActivityNotFoundException a) {
                      Toast.makeText(getApplicationContext(),
                                   "دستگاه شما از این قابلیت پشتیبانی نمی کند",
                              Toast.LENGTH_SHORT).show();
                  }

              }
          } );




        rv_categories=findViewById( R.id.rv_category_main );
        rv_products=findViewById( R.id.rv_product );
        et_search=findViewById( R.id.et_search );
        et_search.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                products.clear();
                products.addAll( dao.filterProducts("%" + editable.toString() + "%" ) );

                if (products.size()==0){

                    tv_no_item.setVisibility( View.VISIBLE );
                }else {

                    tv_no_item.setVisibility( View.INVISIBLE );

                }
                adapterProduct.notifyDataSetChanged();

              // search( editable.toString() );

            }
        } );

       tv_setting.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity( new Intent( MainActivity.this, SettingActivity.class ) );


            }
        } );


        products=dao.getproducts();


       // adapterProduct=new AdapterProduct( MainActivity.this,products );

        //rv_products.setAdapter( adapterProduct );
        rv_products.setLayoutManager( new LinearLayoutManager( MainActivity.this ) );


        categories=dao.getCategories();  //liste dastebany ra az database barmigardanad.



        mainCategoryAdapter=new MainCategoryAdapter( MainActivity.this,categories,this );

        rv_categories.setAdapter( mainCategoryAdapter );

        rv_categories.setLayoutManager( new LinearLayoutManager( MainActivity.this,RecyclerView.HORIZONTAL,false ) );  //chegunegie namayeshe list .

        drawerLayout=findViewById( R.id.drawer_layout );
        iv_hum=findViewById( R.id.iv_hum );
        iv_close_drawer=findViewById( R.id.iv_close_drawer );
        iv_profile=findViewById( R.id.iv_profile_main );


        tv_category=findViewById( R.id.tv_category );
        tv_category.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( MainActivity.this, CategoryActivity.class );
                startActivity(intent);
            }
        } );




        cl_product=findViewById( R.id.cl_product );


        /*iv_close_profile=findViewById( R.id.iv_close_profile );*/


      /*  iv_close_profile.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        } );*/



        cl_product.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity( new Intent( MainActivity.this, NewProductActivity.class ) );

            }
        } );


        iv_hum.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawerLayout.openDrawer( Gravity.RIGHT );

            }
        } );

        iv_close_drawer.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer( Gravity.RIGHT );

            }
        } );

        iv_profile.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent( MainActivity.this, ProfileActivity.class );
                startActivity( intent );
            }
        } );


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult( requestCode, resultCode, data );
        if (requestCode==100 && resultCode==RESULT_OK){
            ArrayList<String> result = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            et_search.setText( result.get( 0 ) );


        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        String profile =sharedPreferences.getString( "profile","" );
        if(!profile.isEmpty()){
            iv_profile.setImageURI( Uri.parse( profile ) );
        }

        products.clear();
        products=dao.getproducts();
        adapterProduct=new AdapterProduct( MainActivity.this,products,this );
        rv_products.setAdapter( adapterProduct );
    }


    public void search(String matn){

        products.clear();
        products.addAll( dao.filterProducts("%" + matn + "%" ) );
        adapterProduct.notifyDataSetChanged();

    }

    @Override
    public void itemClicked(int position) {
        Intent intent=new Intent( MainActivity.this, DetailActivity.class ); //raftan az yek activity be activitye digar.

        intent.putExtra( "idd",products.get( position ).getId() ); //ferestadane id mahsul be activitiye morede nazar.(estelahan bundel).

        startActivity( intent );

    }

    @Override
    public void categoryClicked(int position) {

        if (categories.get( position ).isSelected()){

            categories.get( position ).setSelected( false );
            mainCategoryAdapter.notifyDataSetChanged();
            products.clear();
            products.addAll( dao.getproducts() );
            adapterProduct.notifyDataSetChanged();


        }else {

            for (int i =0;i<categories.size();i++){
                categories.get( i ).setSelected( false );

            }

            categories.get( position ).setSelected( true );
            mainCategoryAdapter.notifyDataSetChanged();


            products.clear();
            products.addAll( dao.getProductsByCatId( categories.get( position ).getId()) );

            adapterProduct.notifyDataSetChanged();

        }



    }
}
