package com.vahid.poop.main;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vahid.poop.R;
import com.vahid.poop.db.Category;

import java.util.List;

public class MainCategoryAdapter extends RecyclerView.Adapter<MainCategoryAdapter.Holder> {

    Context context;
    List<Category>categories;
    OnCategoryClick onCategoryClick;

    public MainCategoryAdapter (Context context, List<Category>categories,OnCategoryClick onCategoryClick){

        this.context=context;
        this.categories=categories;
        this.onCategoryClick=onCategoryClick;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from( context ).inflate( R.layout.costom_category_main,parent,false );

        return new Holder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        holder.tv_name.setText( categories.get( position ).getName() );

        if (categories.get( position ).isSelected()){
             holder.tv_name.setBackgroundResource(R.drawable.bg_category_main_selected  );
             holder.tv_name.setTextColor( Color.parseColor( "#ffffff" ) );
        }else {
            holder.tv_name.setBackgroundResource(R.drawable.bg_category_main );
            holder.tv_name.setTextColor( Color.parseColor( "#242424" ) );


        }

    }


    public interface OnCategoryClick{
        public void categoryClicked(int position);
    }

    @Override
    public int getItemCount()
    {
        return categories.size();
    }

    class Holder extends RecyclerView.ViewHolder{

        TextView tv_name;


        public Holder(@NonNull View itemView) {
            super( itemView );

            tv_name=itemView.findViewById( R.id.tv_custom_main_category );
            itemView.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onCategoryClick.categoryClicked( getAdapterPosition() );

                }
            } );
        }
    }
}
