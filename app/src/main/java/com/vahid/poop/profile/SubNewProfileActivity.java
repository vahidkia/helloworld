package com.vahid.poop.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.vahid.poop.R;

public class SubNewProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_sub_new_profile );
    }
}
