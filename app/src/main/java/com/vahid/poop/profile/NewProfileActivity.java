package com.vahid.poop.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.vahid.poop.R;

public class NewProfileActivity extends AppCompatActivity {

    TextView name,number;

    ImageView imageView;

    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_new_profile );

        name=(TextView)findViewById( R.id.tv4 );
        number=(TextView)findViewById( R.id.tv5 );

        imageView=(ImageView)findViewById( R.id.iv1 );

        button=(Button)findViewById( R.id.btn_new_profile );

        button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent( getApplicationContext(),SubNewProfileActivity.class );

                Pair<View,String> pair1=Pair.create(findViewById( R.id.tv4),"name"  );

                Pair<View,String> pair2=Pair.create(findViewById( R.id.tv5),"number"  );

                Pair<View,String> pair3=Pair.create(findViewById( R.id.iv1),"image"  );


                ActivityOptions activityOptions=ActivityOptions.makeSceneTransitionAnimation( NewProfileActivity.this,pair1,pair2,pair3 );
                startActivity( intent,activityOptions.toBundle() );
            }
        } );




    }
}
