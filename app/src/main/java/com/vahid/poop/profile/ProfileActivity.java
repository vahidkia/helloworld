package com.vahid.poop.profile;
import android.net.Uri;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.vahid.poop.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    Button btn_anbar;


    CircleImageView iv_profile;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_profile );



        iv_profile=findViewById( R.id.iv_profile );

        sharedPreferences=getSharedPreferences( "setting",MODE_PRIVATE );
        editor=sharedPreferences.edit();


        if(!sharedPreferences.getString( "profile","nopic" ).equalsIgnoreCase( "nopic" )){

            String aks=sharedPreferences.getString( "profile","" );
            Uri uri= Uri.parse( aks );
            iv_profile.setImageURI( uri );
        }


        iv_profile.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Dexter.withActivity(ProfileActivity.this  )
                    .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE  )
                        .withListener( new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {

                                  Intent intent = new Intent( Intent.ACTION_PICK );
                                     intent.setType( "image/*" );
                                              startActivityForResult( intent,200 );

                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                Toast.makeText( getApplicationContext(),"برنامه انبار داری به این دسترسی احتیاج دارد"  ,Toast.LENGTH_LONG).show();

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                            }
                        } ).check();

            }
        } );

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult( requestCode, resultCode, data );

        if(requestCode==200&&resultCode==RESULT_OK){

            Uri uri = data.getData();
            iv_profile.setImageURI( uri );
            editor.putString( "profile",uri.toString() );
            editor.apply();
        }
    }
}
