
package com.vahid.poop.categoryfactor;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vahid.poop.R;
import com.vahid.poop.db.Category;
import com.vahid.poop.db.Categoryfactor;

import java.util.List;

public class CategoryfactorAdapter extends RecyclerView.Adapter<CategoryfactorAdapter.Holder> {

    List<Categoryfactor>categoryfactors;
    Context context;

    public CategoryfactorAdapter(Context context, List<Categoryfactor>categoryfactors){
        this.context=context;
        this.categoryfactors=categoryfactors;

    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from( context ).inflate( R.layout.custom_item_category_factor,parent,false );
        return new Holder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        holder.tv_name.setText( categoryfactors.get( position ).getName() );

    }

    @Override
    public int getItemCount() {
        return categoryfactors.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView tv_name;
        ImageView iv_delete, iv_edit;

        public Holder(@NonNull View itemView) {
            super( itemView );
            tv_name=itemView.findViewById( R.id.tv_custom_cat_name_factor );
            iv_delete=itemView.findViewById( R.id.iv_delet_category_factor );
            iv_edit=itemView.findViewById( R.id.iv_edit_category_factor );
        }
    }
}

