package com.vahid.poop.categoryfactor;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.vahid.poop.R;
import com.vahid.poop.category.CategoryActivity;
import com.vahid.poop.db.Categoryfactor;
import com.vahid.poop.db.Dao;
import com.vahid.poop.db.MyDatabase;
import com.vahid.poop.newfactor.NewFactorActivity;

import java.util.ArrayList;
import java.util.List;

public class CategoryFactorActivity extends AppCompatActivity {

    MyDatabase myDatabase;
    Dao dao;

    Button btn_add_factor;
    List<Categoryfactor>categoryfactors=new ArrayList<>(  );

    RecyclerView recyclerView;
    CategoryfactorAdapter categoryfactorAdapter;

    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_category_factor );

        btn_add_factor=findViewById( R.id.btn_add_category_factor1 );

        recyclerView=findViewById( R.id.rv_category_factor );


        myDatabase= Room.databaseBuilder( CategoryFactorActivity.this,
                MyDatabase.class,"anbar")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();

        dao =myDatabase.getDao();
        categoryfactors=dao.getCategoryfactors();

        categoryfactorAdapter=new CategoryfactorAdapter( CategoryFactorActivity.this,categoryfactors );
        recyclerView.setAdapter( categoryfactorAdapter );
        recyclerView.setLayoutManager( new LinearLayoutManager( CategoryFactorActivity.this ) );


        btn_add_factor.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alert =new AlertDialog.Builder( CategoryFactorActivity.this );

                EditText editText=new EditText( CategoryFactorActivity.this );
                alert.setView( editText );
                editText.setHint( "نوع فاکتور" );
                alert.setTitle( "فاکتور جدید" );
                alert.setMessage( "نوع فاکتور را مشخص نمایید:" );
                alert.setPositiveButton( "ذخیره کردن", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       String categoryfactor_name=editText.getText().toString();    // gereftane string dakhele editext
                       Categoryfactor categoryfactor=new Categoryfactor();
                       categoryfactor.setName( categoryfactor_name );

                       long result =dao.insertCategoryFactor(  categoryfactor);

                       if (result>0){
                           Toast.makeText( CategoryFactorActivity.this, "فاکتور جدید ذخیره شد.", Toast.LENGTH_SHORT ).show();
                       }else {
                           Toast.makeText( CategoryFactorActivity.this, "سیستم با خطا مواجه گردید!", Toast.LENGTH_SHORT ).show();

                       }
                    }
                } );
                alert.setNegativeButton( "لغو", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                } );
                alert.show();
            }
        } );
    }
}
