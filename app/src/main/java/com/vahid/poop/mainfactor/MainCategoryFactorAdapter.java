package com.vahid.poop.mainfactor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vahid.poop.R;
import com.vahid.poop.db.Categoryfactor;

import java.util.List;

public class MainCategoryFactorAdapter extends RecyclerView.Adapter<MainCategoryFactorAdapter.Holder> {

    Context context;
    List<Categoryfactor>categoryfactors;
    public MainCategoryFactorAdapter (Context context, List<Categoryfactor>categoryfactors){

        this.context=context;
        this.categoryfactors=categoryfactors;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.custom_category_factor_main,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        holder.tv_name.setText(categoryfactors.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return categoryfactors.size();
    }

    class Holder extends RecyclerView.ViewHolder{

         TextView tv_name;


        public Holder(@NonNull View itemView) {
            super(itemView);
            tv_name=itemView.findViewById(R.id.tv_custom_main_category_factor);
        }
    }
}
