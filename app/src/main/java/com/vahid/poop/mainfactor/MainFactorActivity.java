package com.vahid.poop.mainfactor;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.vahid.poop.R;
import com.vahid.poop.categoryfactor.CategoryFactorActivity;
import com.vahid.poop.db.Categoryfactor;
import com.vahid.poop.db.Dao;
import com.vahid.poop.db.MyDatabase;
import com.vahid.poop.db.NewFactor;
import com.vahid.poop.newfactor.NewFactorActivity;

import java.util.ArrayList;
import java.util.List;

public class MainFactorActivity extends AppCompatActivity implements View.OnClickListener {
    DrawerLayout drawerLayout;
    ImageView imageView;

    ConstraintLayout cl_factor;

   TextView tv_type_factor;

   public FloatingActionButton floatingActionButton;
   public FloatingActionButton floatingActionButton1;

   MyDatabase myDatabase;

   RecyclerView rv_categoryfactors;
   List<Categoryfactor>categoryfactors=new ArrayList<>();
   MainCategoryFactorAdapter mainCategoryFactorAdapter;
   Dao dao;


    // List<NewFactor>newFactors=new ArrayList<>(  );
    // Dao dao;

    ImageView scanBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main_factor );

        scanBtn=findViewById(R.id.iv_scan_factor);
        scanBtn.setOnClickListener(this);

        myDatabase= Room.databaseBuilder(MainFactorActivity.this,
                MyDatabase.class,"anbar")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
        dao=myDatabase.getDao();
        categoryfactors=dao.getCategoryfactors();
        rv_categoryfactors=findViewById(R.id.rv_category_factor_main);

        mainCategoryFactorAdapter=new MainCategoryFactorAdapter(MainFactorActivity.this,categoryfactors);
        rv_categoryfactors.setAdapter(mainCategoryFactorAdapter);

        rv_categoryfactors.setLayoutManager(new LinearLayoutManager(MainFactorActivity.this,RecyclerView.HORIZONTAL,false));

        drawerLayout=findViewById( R.id.drawer_layout );
        imageView=findViewById( R.id.iv_hum );
       // cl_factor=findViewById( R.id.cl_factor );





        tv_type_factor=findViewById( R.id.tv_type_factor );

        floatingActionButton=findViewById(R.id.floatingActionNew);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(MainFactorActivity.this,NewFactorActivity.class);
                startActivity(intent);
            }
        });

        tv_type_factor.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent( MainFactorActivity.this,CategoryFactorActivity.class );
                startActivity( intent );
            }
        } );

        floatingActionButton1=findViewById(R.id.floatingActionBarcod);
        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(MainFactorActivity.this,NewFactorActivity.class);
                startActivity(intent);
            }
        });

     //    newFactors=dao.getnewfactors();


      /*  cl_factor.setOnClickListener( new View.OnClickListener() {              //bejash floating button gozashtim.
            @Override
            public void onClick(View view) {
                startActivity( new Intent( MainFactorActivity.this, NewFactorActivity.class ) );
            }
        } );
*/



        imageView.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer( Gravity.RIGHT );

            }
        } );
    }

    @Override
    public void onClick(View view) {
        scanCode();

    }
    private void scanCode(){
        IntentIntegrator integrator=new IntentIntegrator(this);
        integrator.setCaptureActivity(CaptureAct.class);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("کد را اسکن کنید");
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult (int requestCode ,int resultCode,Intent data){
        IntentResult result=IntentIntegrator.parseActivityResult(requestCode,resultCode,data) ;
        if (result !=null){
            if (result.getContents() !=null){
                AlertDialog.Builder builder=new AlertDialog.Builder(this);
                builder.setMessage(result.getContents());
                builder.setTitle("نتیجه اسکن");
                builder.setPositiveButton("دوباره اسکن کنید", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        scanCode();

                    }
                }).setNegativeButton("پایان", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                AlertDialog dialog=builder.create();
                dialog.show();
            }
            else {
                Toast.makeText(this, "نتیجه ای نداشت", Toast.LENGTH_SHORT).show();
            }
        }else {
            super.onActivityResult(requestCode,resultCode,data);
        }
    }
}
