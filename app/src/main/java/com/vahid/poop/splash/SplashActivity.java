package com.vahid.poop.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.vahid.poop.LoginActivity;
import com.vahid.poop.R;
import com.vahid.poop.main.MainActivity;

public class SplashActivity extends AppCompatActivity {

     SharedPreferences sharedPreferences;

    boolean login_state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_splash );

        sharedPreferences=getSharedPreferences( "setting",MODE_PRIVATE );
         login_state=sharedPreferences.getBoolean( "login",false );


        new Handler().postDelayed( new Runnable() {
            @Override
            public void run() {

                if(login_state){
                    Intent intent=new Intent( SplashActivity.this,MainActivity.class );
                    startActivity(intent);
                }else {
                    Intent intent=new Intent( SplashActivity.this, LoginActivity.class );
                    startActivity(intent);

                }




            }
        },2500 );


    }
}
