package com.vahid.poop.db;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Categoryfactor {

    @PrimaryKey(autoGenerate = true)
    int id;


    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
