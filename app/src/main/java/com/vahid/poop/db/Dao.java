package com.vahid.poop.db;


import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@androidx.room.Dao
public interface Dao {

    @Insert
    public long insertCategory(Category category);   //vared kardane data be jadval.long akharin satry ke vared shode id anra be ma bar migardanad.

    @Query( "select * from category order by id desc" )
    public List<Category> getCategories();         // har moghe seda zade shavad maghadire jadval ra bar migardanad.


    @Insert
    public long insertProduct(Product product);   //metode vared kardane data be jadvale product.

    @Query( "select * from product" )             //mahsulati ke dar database darim ra bar migardanad.
    public List<Product> getproducts();            // * yany hameye sutunhaye jadval (mitavanim bejaye setare name sutunhaye delkhah ra vared konim)

    @Query( "select * from product where id=:id" )
    public Product getDetail(int id);

    @Query( "select * from product where cat_id=:cat_id" )
    public List<Product>getProductsByCatId(int cat_id);

    @Query("select * from product where name like :search " )
    public List<Product>filterProducts(String search);

    @Query( "delete  from category where id=:id" )
    public int deletecategory(int id);

    @Query( "update category set name=:name where id=:id" )
    public int updateCategory(String name , int id);





    @Insert
    public long insertCategoryFactor(Categoryfactor categoryfactor);

    @Query( "select * from categoryfactor" )
    public List<Categoryfactor> getCategoryfactors();

    @Insert
    public long insertNewffactor(NewFactor newfactor);

    @Query( "select * from newfactor" )
    public List<NewFactor>getnewfactors();
}
