package com.vahid.poop.db;


import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey( onDelete = CASCADE,
        entity = Categoryfactor.class,
        parentColumns ="id",
        childColumns = "catt_id"))
public class NewFactor {


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBarcod() {
        return barcod;
    }

    public void setBarcod(int barcod) {
        this.barcod = barcod;
    }

    public long getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(long unit_price) {
        this.unit_price = unit_price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getCatt_id() {
        return catt_id;
    }

    public void setCatt_id(int catt_id) {
        this.catt_id = catt_id;
    }

    public int getEnd_price() {
        return end_price;
    }

    public void setEnd_price(int end_price) {
        this.end_price = end_price;
    }

    public NewFactor(String name, int barcod, long unit_price, String color, String company, int number, int catt_id, int end_price) {
        this.name = name;
        this.barcod = barcod;
        this.unit_price = unit_price;
        this.color = color;
        this.company = company;
        this.number = number;
        this.catt_id = catt_id;
        this.end_price = end_price;
    }

    @PrimaryKey(autoGenerate = true)
    int id;

    String name;

    int barcod;

    long unit_price;

    String color;

    String company;

    int number;

    int catt_id;

    int end_price;
}
