package com.vahid.poop.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

// har jadvali ke bekhahim be data base ezafe beshavad dakhele akolad mizanim.

@Database(entities = {Category.class,Product.class,Categoryfactor.class,NewFactor.class},version = 5 ,exportSchema = false)
public abstract class MyDatabase extends RoomDatabase {

    public abstract  Dao getDao();       //bargardandane kelase Dao
}
