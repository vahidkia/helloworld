package com.vahid.poop.db;


import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

// vasl kardane id jadvale category be id jadvale product
@Entity(foreignKeys = @ForeignKey( onDelete = CASCADE,
        entity = Category.class,
         parentColumns ="id",
         childColumns = "cat_id"))


public class Product {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getInventory() {
        return inventory;
    }

    public void setInventory(int inventory) {
        this.inventory = inventory;
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    // kelase constractor (metode sazandeh ) meghdar dehee hame motaghayerha besurate yekja.
    public Product(String name, String pic, long price, String color, String company, int inventory, int cat_id) {
        this.name = name;
        this.pic = pic;
        this.price = price;
        this.color = color;
        this.company = company;
        this.inventory = inventory;
        this.cat_id = cat_id;
    }


    @PrimaryKey(autoGenerate = true)  // id besurate automatic ezafe mikonad

    int id;

    String name;

    String pic;

    long price;

    String color;

    String company;

    int inventory;

    int cat_id;

    String des;
}
