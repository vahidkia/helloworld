package com.vahid.poop.Product;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.room.Room;

import android.Manifest;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.vahid.poop.R;
import com.vahid.poop.category.CategoryActivity;
import com.vahid.poop.db.Category;
import com.vahid.poop.db.Dao;
import com.vahid.poop.db.MyDatabase;
import com.vahid.poop.db.Product;
import com.vahid.poop.main.MainActivity;
import com.vahid.poop.profile.ProfileActivity;

import java.util.ArrayList;
import java.util.List;

public class NewProductActivity extends AppCompatActivity {


    MyDatabase myDatabase;    //sakhte database va gereftane listeman.
    Dao dao;

    List<Category> categories=new ArrayList<>();

    List<String> spinner_items=new ArrayList<>();

    Spinner spinner;

    ImageView iv_gallery;

    String pic_address;

    EditText et_name, et_color,et_inventory,et_company,et_des,et_price,et_location;

    Button btn_submit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_new_product );



        ConstraintLayout constraintLayout=findViewById( R.id.layout_new_product );
        AnimationDrawable animationDrawable=(AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration( 2000 );
        animationDrawable.setExitFadeDuration( 4000 );
        animationDrawable.start();








        iv_gallery=findViewById( R.id.iv_gallery_product );

        et_name=findViewById( R.id.et_name_product );
        et_color=findViewById( R.id.et_color_price );
        et_price=findViewById( R.id.et_price_product );
        et_company=findViewById( R.id.et_company_product );
        et_inventory=findViewById( R.id.et_inventory_product );
        et_des=findViewById( R.id.et_des_product );
       // et_location=findViewById( R.id.et_location_product );

        btn_submit=findViewById( R.id.btn_new_product1 );



        btn_submit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name=et_name.getText().toString();
                String color=et_color.getText().toString();
                String price=et_price.getText().toString();
                String inventory= et_inventory.getText().toString();
                String company = et_company.getText().toString();
                String des=et_des.getText().toString();

               // String location=et_location.getText().toString();

                if(name.isEmpty()){
                    Toast.makeText( NewProductActivity.this, "لطفا نام محصول را وارد نمایید.", Toast.LENGTH_SHORT ).show();
                    et_name.requestFocus();
                }else if(color.isEmpty()){
                    Toast.makeText( NewProductActivity.this, "لطفا رنگ محصول را وارد نمایید.", Toast.LENGTH_SHORT ).show();
                    et_color.requestFocus();
                }else if (inventory.isEmpty()){
                    Toast.makeText( NewProductActivity.this, "لطفا موجودی محصول را وارد نمایید.", Toast.LENGTH_SHORT ).show();
                    et_inventory.requestFocus();
                }else if (company.isEmpty()){
                    Toast.makeText( NewProductActivity.this, "لطفانام شرکت تولید کننده محصول را وارد نمایید.", Toast.LENGTH_SHORT ).show();
                    et_company.requestFocus();
                }else {
                        int id=categories.get( spinner.getSelectedItemPosition()).getId();

                        Product product=new Product( name,pic_address,Integer.valueOf( price ),color,company,Integer.valueOf( inventory ),id ); //sakhte yek nemine az product.
                        long result=dao.insertProduct( product );

                        if (result>0){
                            Toast.makeText( NewProductActivity.this, "محصول جدید ذخیره شد.", Toast.LENGTH_SHORT ).show();
                            onBackPressed();
                        }

                }


            }
        } );



        myDatabase= Room.databaseBuilder( NewProductActivity.this,
                MyDatabase.class,"anbar")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
        dao =myDatabase.getDao();

        categories=dao.getCategories();

        spinner=findViewById(R.id.spinner_product);

        for (int i=0; i<categories.size();i++){
            spinner_items.add(categories.get(i).getName());
        }


        // baraye load shodan liste spinner
        ArrayAdapter<String> spinner_adapter=new ArrayAdapter<String>(NewProductActivity.this,
                android.R.layout.simple_spinner_item,spinner_items);
        spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter( spinner_adapter );

        iv_gallery.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Dexter.withActivity( NewProductActivity.this  )
                        .withPermission( Manifest.permission.READ_EXTERNAL_STORAGE  )
                        .withListener( new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {

                                Intent intent = new Intent( Intent.ACTION_PICK );
                                intent.setType( "image/*" );
                                startActivityForResult( intent,200 );

                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                Toast.makeText( getApplicationContext(),"برنامه انبار داری به این دسترسی احتیاج دارد"  ,Toast.LENGTH_LONG).show();

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                            }
                        } ).check();


            }
        } );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult( requestCode, resultCode, data );

        if(requestCode==200 && resultCode==RESULT_OK);

        Uri uri=data.getData();
        iv_gallery.setImageURI(uri);
        pic_address=uri.toString();

    }
}


  // vaghti activitye product baz mishavad :liste categoryha az database mikhanim ke rikhte mishavad
   // dar liste category ke sakhtim.kebayad be spinner dade shavad.

  //  baraye meghdardehi be spineer ,niyaz be yek list az noee string mibashad.