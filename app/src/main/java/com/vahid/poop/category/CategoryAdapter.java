package com.vahid.poop.category;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.vahid.poop.R;
import com.vahid.poop.db.Category;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.Holder> {

    //tarife list (metode sazande)

    List<Category> categories;
    Context context;
    CategoryItemClickHandle categoryItemClickHandle;

       public CategoryAdapter(Context context, List<Category> categories,CategoryItemClickHandle categoryItemClickHandle){
           this.context=context;
           this.categories=categories;
           this.categoryItemClickHandle=categoryItemClickHandle;

       }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
           //tarif inke kelase Holder be kodam laye dastresi peyda konad.

           View view= LayoutInflater.from( context ).inflate( R.layout.custom_item_category,parent,false );
        return new Holder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

           holder.tv_name.setText( categories.get( position ).getName() );

           if (position==0){
                holder.itemView.setBackgroundColor( Color.parseColor( "#f2f2f2" ) );
           }else {

               if(position % 2==0){
                   holder.itemView.setBackgroundColor( Color.parseColor( "#f2f2f2" ) );

               }else {
                   holder.itemView.setBackgroundColor( Color.parseColor( "#ffffff" ) );

               }
           }

           if (categories.get( position ).isDeleted()){
              holder.iv_delete.setVisibility( View.GONE );
              holder.lottieAnimationView.setVisibility( View.VISIBLE );
              holder.lottieAnimationView.playAnimation();
           }else {

               holder.iv_delete.setVisibility( View.VISIBLE );
               holder.lottieAnimationView.setVisibility( View.GONE );
               holder.lottieAnimationView.cancelAnimation();
           }

    }


    public interface CategoryItemClickHandle{         //handle kardane iiconhaye satlashghal va medad.
        public void deleteClicked(int position) ;
        public void  editClicked(int position);



    }


    @Override
    public int getItemCount() {         //gereftane tedade satrhaye list

        return categories.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

           TextView tv_name;

           ImageView iv_delete ,iv_edit;

           LottieAnimationView lottieAnimationView;

        public Holder(@NonNull View itemView) {
            super( itemView );

            tv_name=itemView.findViewById( R.id.tv_custom_cat_name );

            iv_delete=itemView.findViewById( R.id.iv_delet_category );

            iv_edit=itemView.findViewById(R.id.iv_edit_category  );
             lottieAnimationView=itemView.findViewById( R.id.lottie_delete );

            iv_delete.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    categoryItemClickHandle.deleteClicked( getAdapterPosition() );

                }
            } );

            iv_edit.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    categoryItemClickHandle.editClicked( getAdapterPosition() );


                }
            } );

        }
    }
}
