package com.vahid.poop.category;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.vahid.poop.R;
import com.vahid.poop.db.Category;
import com.vahid.poop.db.Dao;
import com.vahid.poop.db.MyDatabase;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends AppCompatActivity implements CategoryAdapter.CategoryItemClickHandle {

    MyDatabase myDatabase;
    Dao dao;

    Button btn_add;

    List<Category> categories=new ArrayList<>();    //gereftan hame categoriha besurate yek list.

    RecyclerView recyclerView;

    CategoryAdapter categoryAdapter;

    TextView tv_no_item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_category );

        btn_add=findViewById( R.id.btn_add_category1 );

        tv_no_item=findViewById( R.id.tv_noitem_category );

        recyclerView=findViewById( R.id.rv_category );

        myDatabase= Room.databaseBuilder( CategoryActivity.this,
                  MyDatabase.class,"anbar")
                  .allowMainThreadQueries()
                   .fallbackToDestructiveMigration()
                   .build();

        dao =myDatabase.getDao();

        categories=dao.getCategories();          // seda zadan metode tarif shode.


        if(categories.size()>0){
            tv_no_item.setVisibility( View.INVISIBLE );

        }

        categoryAdapter=new CategoryAdapter( CategoryActivity.this,categories,this );     //sakhte yek nemune az kelas.

        recyclerView.setAdapter( categoryAdapter );

        recyclerView.setLayoutManager( new LinearLayoutManager( CategoryActivity.this ) ); //namayesh list besurate khatye sadeh.

        btn_add.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert=new AlertDialog.Builder( CategoryActivity.this );

                final EditText editText=new EditText( CategoryActivity.this );
                alert.setView( editText );
                editText.setHint( "عنوان دسته جدید" );
                alert.setTitle( "دسته جدید" );
                alert.setMessage( "نام دسته مورد نظر را وارد نمایید" );
                alert.setPositiveButton( "ذخیره ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String category_name=editText.getText().toString();
                        Category category=new Category();
                        category.setName( category_name );

                        long result =dao.insertCategory( category );
                        if (result>0){

                            categories.add(0 ,category );
                            categoryAdapter.notifyDataSetChanged();
                            Toast.makeText( CategoryActivity.this, "دسته بندی ذخیره گردید", Toast.LENGTH_SHORT ).show();
                        }else{
                            Toast.makeText( CategoryActivity.this, "خطا در ذخیره دسته بندی ", Toast.LENGTH_SHORT ).show();

                        }
                    }
                } );

                alert.setNegativeButton( "لغو", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                } );
                alert.show();

            }
        } );
    }

    @Override
    public void deleteClicked(int position) {
        AlertDialog.Builder alert=new AlertDialog.Builder( CategoryActivity.this );
        alert.setTitle( "حذف دسته بندی" );
        alert.setMessage( "دسته بندی حذف شود؟" );
        alert.setPositiveButton( "بله", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                int result=dao.deletecategory(categories.get( position ).getId()  );   //pak kardane dastebandy.

                if (result>0){

                    categories.get( position ).setDeleted( true );
                    categoryAdapter.notifyItemChanged( position  );
                    Toast.makeText( CategoryActivity.this, "حذف دسته باموفقیت انجام شد.", Toast.LENGTH_SHORT ).show();
                    new Handler(  ).postDelayed( new Runnable() {
                        @Override
                        public void run() {
                            categories.remove( position );
                            categoryAdapter.notifyItemRemoved( position );

                        }
                    },1500 );

                }
            }
        } );

        alert.setNegativeButton( "خیر", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        } );

        alert.show();
      //  Toast.makeText( this, "pak", Toast.LENGTH_SHORT ).show();
    }

    @Override
    public void editClicked(int position) {


        AlertDialog.Builder alert=new AlertDialog.Builder( CategoryActivity.this );

        EditText editText=new EditText( this );
        editText.setHint( "نام دسته بندی جدید را وارد کنید" );
        alert.setView( editText );

        alert.setTitle( "به روز رسانی دسته بندی" );
        alert.setMessage( "ایا مایل به به روزرسانی دسته بندی میباشید؟" );
        alert.setPositiveButton( "بله", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                String new_name =editText.getText().toString();
                int result=dao.updateCategory( new_name,categories.get( position ).getId() );   //ic_edittt kardane dastebandy.

                if (result>0){

                    Toast.makeText( getApplicationContext(), " به روز رسانی انجام شد.", Toast.LENGTH_SHORT ).show();
                    categories.get( position ).setName( new_name );
                    categoryAdapter.notifyItemChanged( position );
                }
            }
        } );

        alert.setNegativeButton( "خیر", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        } );

        alert.show();

    }
}
