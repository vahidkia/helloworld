package com.vahid.poop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vahid.poop.profile.NewProfileActivity;
import com.vahid.poop.profile.ProfileActivity;

public class LoginActivity extends AppCompatActivity {

    Button btn_login;
    EditText et_username  ,  et_password;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login );




        ConstraintLayout constraintLayout=findViewById( R.id.layout_login );
        AnimationDrawable animationDrawable=(AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration( 2000 );
        animationDrawable.setExitFadeDuration( 4000 );
        animationDrawable.start();









        sharedPreferences=getSharedPreferences( "setting",MODE_PRIVATE );
        editor=sharedPreferences.edit();

        btn_login=findViewById( R.id.btn_logine );
        et_username=findViewById( R.id.et_username );
        et_password=findViewById( R.id.et_password );

        btn_login.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username= et_username.getText().toString();
                String password = et_password.getText().toString();

                if(username.length()==0 || password.length()==0) {
                    Toast.makeText( LoginActivity.this," نام کاربری یا رمز عبور خالی میباشد" ,Toast.LENGTH_LONG ).show();
                }else
                    if(username.equals( "admin" ) && password.equals( "123456" )){
                        Intent intent=new Intent( LoginActivity.this, ProfileActivity.class );
                        startActivity( intent );
                        editor.putBoolean( "login",true );
                        editor.apply();
                        finish();
                        Toast.makeText( LoginActivity.this,"خوش آمدید" ,Toast.LENGTH_LONG ).show();
                    }else{
                        Toast.makeText( LoginActivity.this," نام کاربری یا رمز عبور اشتباه میباشد" ,Toast.LENGTH_LONG ).show();
                    }

            }
        } );
    }
}
