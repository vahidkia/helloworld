package com.vahid.poop.newfactor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.vahid.poop.R;
import com.vahid.poop.categoryfactor.CategoryFactorActivity;
import com.vahid.poop.db.Categoryfactor;
import com.vahid.poop.db.Dao;
import com.vahid.poop.db.MyDatabase;
import com.vahid.poop.db.NewFactor;

import java.util.ArrayList;
import java.util.List;

public class NewFactorActivity extends AppCompatActivity {

    MyDatabase myDatabase;
    Dao dao;

    List<Categoryfactor> categoryfactors=new ArrayList<>(  );


    List<String>spiner_items=new ArrayList<>(  );  //baraye inke spiner list az jens string mikhahad.
    Spinner spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_new_factor );


        myDatabase= Room.databaseBuilder( NewFactorActivity.this,
                MyDatabase.class,"anbar")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();

        dao =myDatabase.getDao();
        categoryfactors=dao.getCategoryfactors();

        spinner=findViewById( R.id.spinner_factor );

        for (int i=0; i<categoryfactors.size();i++){

            spiner_items.add( categoryfactors.get( i ).getName() );
        }

        ArrayAdapter<String> spinner_adapter=new ArrayAdapter<String>(NewFactorActivity.this,
                android.R.layout.simple_spinner_item,spiner_items);
                spinner_adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        spinner.setAdapter( spinner_adapter );
    }
}
