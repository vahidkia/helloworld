package com.vahid.poop.detail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vahid.poop.R;
import com.vahid.poop.db.Dao;
import com.vahid.poop.db.MyDatabase;
import com.vahid.poop.db.Product;
import com.vahid.poop.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DetailActivity extends AppCompatActivity {

    Dao dao;
    MyDatabase myDatabase;

    @BindView( R.id.iv_detail )
    ImageView imageView;

    @BindView( R.id.tv_name_detail )
    TextView tv_name;

    @BindView( R.id.tv_color_detail )
    TextView tv_color;

    @BindView( R.id.tv_company_detail )
    TextView tv_company;

    @BindView( R.id.tv_price_detail )
    TextView tv_price;

    @BindView( R.id.tv_inventory_detail )
    TextView tv_inventory;

    @BindView( R.id.tv_detail_des)
    TextView tv_des;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_detail );

        ButterKnife.bind( this );

        int id=getIntent().getIntExtra( "idd",0 );

       myDatabase= Room.databaseBuilder( DetailActivity.this,
                MyDatabase.class,"anbar")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();

        dao=myDatabase.getDao();

        Product product=dao.getDetail( id );

        tv_name.setText( product.getName() );
        tv_color.setText( product.getColor() );
        imageView.setImageURI( Uri.parse( product.getPic() ) );
        tv_price.setText( product.getPrice()+" " );
        tv_company.setText( product.getCompany() );

    }
}
